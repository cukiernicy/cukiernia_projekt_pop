<?php

namespace App\Controller\Admin;

use App\Entity\Plan;
use App\Repository\UserRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PlanCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Plan::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
    public function configureFields(string $pageName): iterable
    {
        //odfiltrowanie tylko tych użytkowników z rolą user
        $pracownik=AssociationField::new('pracownik_id_pracownika', 'Pracownik')
            ->setFormTypeOption('query_builder', function (UserRepository $userRepository){
                return $userRepository->createQueryBuilder('u')
                    ->andWhere('u.roles LIKE :role')->setParameter('role', '%"ROLE_WORKER"%');
            });
        //koniec odfiltrowywnia użytkowników

        return [
            TextField::new('poniedzialek', 'Poniedziałek: '),
            TextField::new('wtorek', 'Wtorek: '),
            TextField::new('sroda', 'Środa: '),
            TextField::new('czwartek', 'Czwartek: '),
            TextField::new('piatek', 'Piątek: '),
            $pracownik //wywołanie klucza obcego
        ];
    }

}
