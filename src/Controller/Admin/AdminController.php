<?php


namespace App\Controller\Admin;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminController implements EventSubscriberInterface   //UserCrudController wywołuje
    //to poprzez plik /config/packages/easy_admin.yaml
    //gdzie edytujemy wybraną instancje wskazując controller
    //który obsługuje EventSubscriberInterface
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    private $entityManager;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager)
    {
        $this->passwordEncoder = $passwordEncoder;  //to konstruktor klasy AdminController, to ustawiasz, żeby móc używać UserPasswordEncoderInterface przy pomocy $this
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()    //to jest najważniejsza metoda bo to pochodna klasy EventSubscriberInterface, ona wyłapuje te eventy, które występują w CrudControllers
    {
        return [
            BeforeEntityPersistedEvent::class => ['encodePassword'],//to jest ważne BeforeEntity persisted mówi nam,
            //że wykonujemy metodę encodePassword przed wysłaniem nowej encji do bazy danych
            BeforeEntityUpdatedEvent::class => ['encodePasswordOnUpdate'],
        ];
    }

    public function encodePassword(BeforeEntityPersistedEvent $builtEvent)  //to jest metoda wywoływana w metodzie getSubscribedEvents()
        //za argumenty przyjmuje obiekt klasy BeforeEntityPersistedEvent
        //bo użyliśmy właśnie tej klasy w metodzie getSubscribedEvents()
        //to jest takie połączenie między tymi dwiema metodami
    {
        $user = $builtEvent->getEntityInstance();   //pobierasz instancje encji, którą tworzysz, edytujesz
        if (!$user instanceof User) {                //jesli encja nie jest instancją klasy User
            return;                                 //to wracasz skąd przyszłaś
        }                                           //ale jeśli jest
        $user->setPassword(                         //to ustawiasz użytkownikowi hasło poprzez jego metodę setPassword
            $this->passwordEncoder->encodePassword($user, $user->getPassword())     //tutaj wywołujesz passwordEncoder (ktory wlasnie hashuje to haslo),
        //przyjmuje argumenty ($user, $user->getPassword())
        //$user to instancja usera, ktorego wlasnie tworzysz
        //a $user->getPassword() to pobranie niezahashowanego hasla, ktore wprowadzilas np. 123
        //i właśnie jak juz ma te dwie rzeczy to hashuje sobie to $user->getPassword (czyli to 123)
        //i ustawia je tej instancji $user
        );


    }

    public function doesUserExist(User $user){
        $allUsers = $this->entityManager->getRepository(User::class)->findAll();
        foreach ($allUsers as $singleUser){
            if($singleUser->getEmail() == $user->getEmail()){
                return true;
            }
        }
        return true;
    }

    public function encodePasswordOnUpdate(BeforeEntityUpdatedEvent $updatedEvent)
    {
        {
            $user = $updatedEvent->getEntityInstance();   //pobierasz instancje encji, którą tworzysz, edytujesz
            if (!$user instanceof User) {                //jesli encja nie jest instancją klasy User
                return;                                 //to wracasz skąd przyszłaś
            }                                           //ale jeśli jest
            $user->setPassword(                         //to ustawiasz użytkownikowi hasło poprzez jego metodę setPassword
                $this->passwordEncoder->encodePassword($user, $user->getPassword())     //tutaj wywołujesz passwordEncoder (ktory wlasnie hashuje to haslo),
            //przyjmuje argumenty ($user, $user->getPassword())
            //$user to instancja usera, ktorego wlasnie tworzysz
            //a $user->getPassword() to pobranie niezahashowanego hasla, ktore wprowadzilas np. 123
            //i właśnie jak juz ma te dwie rzeczy to hashuje sobie to $user->getPassword (czyli to 123)
            //i ustawia je tej instancji $user
            );


        }
    }

}