<?php

namespace App\Controller\Admin;

use App\Entity\Zamowienie;
use App\Repository\UserRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ZamowienieCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Zamowienie::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */

    public function configureFields(string $pageName): iterable
    {
        //odfiltrowanie użytkowników z rolą ROLE_USER
        $klient=AssociationField::new('user_id_usera', 'Klient')
            ->setFormTypeOption('query_builder', function (UserRepository $userRepository) {
            return $userRepository->createQueryBuilder('u')
            ->andWhere('u.roles LIKE :role')->setParameter('role','%"ROLE_USER"%');
        });

        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('oferta_id_oferty', 'Nazwa oferty'),
            $klient,
            DateField::new('data_przyjecia'),
            DateField::new('data_oddania'),
            ChoiceField::new('status')->setChoices(
                ['Nowe zamówienie'=>'0', 'W realizacji'=>'1', 'Gotowe'=>'2']
            ),
            NumberField::new('waga')->formatValue(function ($value) {
                return ($value <0  ) ? sprintf('Liczbs %d nie może być ujemna', $value) : $value;
            }),
        ];
    }
}
