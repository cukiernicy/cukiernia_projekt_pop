<?php

namespace App\Controller\Admin;

use App\Entity\ZamowieniePracownik;
use App\Repository\UserRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class ZamowieniePracownikCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ZamowieniePracownik::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
    public function configureFields(string $pageName): iterable
    {
        $pracownik=AssociationField::new('pracownik_id_pracownika', 'Email pracownika')
            ->setFormTypeOption('query_builder', function (UserRepository $userRepository){
                return $userRepository->createQueryBuilder('u')
                    ->where('u.roles LIKE :role')->setParameter('role', '%"ROLE_WORKER"%');
            });

        return [
            $pracownik,
            AssociationField::new('zamowienie_id_zamowienia', 'Id zamówienia'),
        ];
    }
}
