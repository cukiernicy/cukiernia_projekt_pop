<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */

    public function configureFields(string $pageName): iterable
    {
        $emailField = TextField::new('email', 'Email');

        return [
            TextField::new('name', 'Imie'),
            TextField::new('surname', 'Nazwisko'),
            $emailField,
            TextField::new('tel', 'Telefon'),
            TextField::new('password', 'Hasło'),
            //ArrayField::new('roles', 'Rola')
            ChoiceField::new('roles', 'Rola')->allowMultipleChoices(true)
                ->setChoices(
                [
                    'ROLE_ADMIN'=>'ROLE_ADMIN',
                    'ROLE_USER'=>'ROLE_USER',
                    'ROLE_WORKER'=>'ROLE_WORKER'
                ]
            ),
            TextField::new('miejscowosc', 'Miejscowość')->setRequired(false),
            TextField::new('ulica', 'Ulica')->setRequired(false),
            TextField::new('kod_pocztowy', 'Kod pocztowy')->setRequired(false),
            TextField::new('nr_domu', 'Numer domu')->setRequired(false),
            TextField::new('nr_lokalu', 'Numer lokalu')->setRequired(false),

        ];

    }
}
