<?php

namespace App\Controller\Admin;

use App\Entity\Oferta;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class OfertaCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Oferta::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */

    public function configureFields(string $pageName): iterable
    {
        $imageFile=ImageField::new('obrazek')->setFormType(VichImageType::class);
        $image=ImageField::new('url_obrazka')->setBasePath('/build/');
        $fields= [
            TextField::new('nazwa_oferty'),
            TextEditorField::new('opis_oferty'),
            NumberField::new('cena_za_kg', 'Cena za kg(zł)'),
            //ImageField::new('obrazek')->setFormType(VichImageType::class),
            //ImageField::new('url_obrazka')->setBasePath('/images/products/'),

        ];
        if($pageName == Crud::PAGE_INDEX || $pageName == Crud::PAGE_DETAIL)
        {
            $fields[]=$image;
        }
        else
        {
            $fields[]=$imageFile;
        }
        return $fields;
    }

}
