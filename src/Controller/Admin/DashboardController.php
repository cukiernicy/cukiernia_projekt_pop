<?php

namespace App\Controller\Admin;

use App\Entity\AdresKlienta;
use App\Entity\Oferta;
use App\Entity\Plan;
use App\Entity\User;
use App\Entity\Zamowienie;
use App\Entity\ZamowieniePracownik;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        //return parent::index();
        // redirect to some CRUD controller
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(UserCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Cukiernia Projekt Pop');
    }

    public function configureMenuItems(): iterable
    {
        //yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linktoRoute('Home', 'fa fa-home', 'strona_glowna');
        yield MenuItem::section('User');
        yield MenuItem::linkToCrud('Użytkownik', 'fa fa-user', User::class);
        //yield MenuItem::linkToCrud('Adres', 'fa fa-home', AdresKlienta::class);
        yield MenuItem::linkToCrud('Oferta', 'fa fa-birthday-cake', Oferta::class);

        yield MenuItem::section('Zamówienie');
        yield MenuItem::linkToCrud('Zamówienie', 'fas fa-cookie-bite', Zamowienie::class);
        yield MenuItem::linkToCrud('Zamówienia pracownika', 'fas fa-cookie-bite', ZamowieniePracownik::class);

        yield MenuItem::section('Plan');
        yield MenuItem::linkToCrud('Plan', 'fas fa-calendar-alt', Plan::class);


    }


}
