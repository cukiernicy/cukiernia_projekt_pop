<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
class KontaktController extends AbstractController
{
    /**
     * @Route("/kontakt", name="kontakt")
     */
    public function show()
    {
        $token = $this->get('security.token_storage')->getToken()->getUser();//pobranie adresu email lub anon. jeżeli nie jest zalogowany
        return $this->render('kontakt.html.twig', ['user'=>$token]);
    }
}