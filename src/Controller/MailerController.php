<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class MailerController extends AbstractController
{
    /**
     * @Route("/email")
     */
    public function sendEmail(MailerInterface $mailer)
    {
        $token=$this->get('security.token_storage')->getToken()->getUser()->getUsername();
        $email = (new TemplatedEmail())
            ->from($token)
            ->to('bartek@gmail.com')
            ->htmlTemplate('emails/message.html.twig');

        $mailer->send($email);

        return $this->render("emails/message.html.twig");
    }
}