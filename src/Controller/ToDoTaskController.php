<?php


namespace App\Controller;

use App\Entity\Zamowienie;
use App\Entity\User;
use App\Entity\ZamowieniePracownik;
use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class ToDoTaskController extends AbstractController
{
    /**
     * @Route ("/doRealizacji", name="do_realizacji")
     */
    public function wyswietlZadania(){
        $token = $this->get('security.token_storage')->getToken()->getUser();
        if(!$token->getRoles()){
            $error_message = 'Nie masz odpowiednich uprawnień.';
            return $this->render('error_msg/error_msg.html.twig', ['user'=>$token, 'error_message'=>$token->getRoles()]);
        }
        $zadania = $this->getDoctrine()->getRepository(Zamowienie::class)->findAll();
        return $this->render('worker/zad_do_realizacji.html.twig', ['user'=>$token, 'zadania'=>$zadania]);
    }

    /**
     * @Route ("/doRealizaji/zadania", name="zadania", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function wybraneZadania(Request $request){
        if($request->isXmlHttpRequest()){
            $data = $request->request->get('zadanka');
            $token = $this->get('security.token_storage')->getToken()->getUser();
            $entityManager = $this->getDoctrine()->getManager();
            $userId = $this->getDoctrine()->getRepository(User::class)->find($token);

            if(!$data){
                return new Response("Nic tu nie ma", 500);
            }
            foreach ($data as $key){

                $zamowienieId = $this->getDoctrine()->getRepository(Zamowienie::class)->find($key);
                $zamPr = new ZamowieniePracownik();
                $zamPr->setPracownikIdPracownika($userId);
                $zamowienieId->setStatus(1);
                $zamPr->setZamowienieIdZamowienia($zamowienieId);
                if ($this->isInZamowieniePracownik($key)){
                    return new Response("Zamówienie już przypisane", 500);
                }
                else{
                    $entityManager->persist($zamPr);
                    $entityManager->flush();
                }
            }

            return new Response("Sukces", 200);
        }
        return new Response("This is not Ajax", 400);
    }

    private function isInZamowieniePracownik($id){
        return $this->getDoctrine()->getRepository(ZamowieniePracownik::class)->findBy(array('zamowienie_id_zamowienia'=>$id)) ? true:false;
    }
}