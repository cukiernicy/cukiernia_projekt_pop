<?php


namespace App\Controller;


use App\Entity\Plan;
use App\Entity\User;
use App\Entity\Zamowienie;
use App\Entity\ZamowieniePracownik;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MojPlanController extends AbstractController
{
    /**
     * @Route("/moj_plan", name="moj_plan")
     */
    public function show()
    {
        $token=$this->get('security.token_storage')->getToken()->getUser();

        if(!$token->getRoles()){
            $error_message = 'Nie masz odpowiednich uprawnień.';
            return $this->render('error_msg/error_msg.html.twig', ['user'=>$token, 'error_message'=>$token->getRoles()]);
        }
        $repositoryPlan=$this->getDoctrine()->getRepository(Plan::class);
        $plan=$repositoryPlan->findAll();
        $repositoryUser=$this->getDoctrine()->getRepository(User::class);
        $query=$repositoryUser->createQueryBuilder('u')
            ->where('u.roles LIKE :rola')
            ->setParameter('rola', '%"ROLE_WORKER"%')
            ->getQuery();
        $pracownicy=$query->getResult();

        $repositoryUser=$this->getDoctrine()->getRepository(User::class);


        $repositoryZamowieniePracownik=$this->getDoctrine()->getRepository(ZamowieniePracownik::class);
        $przypisaniPracownicy=$repositoryZamowieniePracownik->findAll();




        return $this->render('worker/mojPlan.html.twig', ['user'=>$token, 'plan'=>$plan, 'pracownicy'=>$pracownicy, 'zamowienia'=>$przypisaniPracownicy]);
    }

    /**
     * @Route("/update_order", name="update_order", methods={"POST"})
     * @param Request $request
     */
    public function update_order(Request $request)
    {
        if($request->isXMLHttpRequest())
        {
            $id=$request->request->get('id');
            $data_oddania=$request->request->get('data_oddania');
            $status=$request->request->get('status');

            $token=$this->get('security.token_storage')->getToken()->getUser();

            $repositoryZamowienia=$this->getDoctrine()->getRepository(Zamowienie::class);
            $zamowienie=$repositoryZamowienia->find($id);
            $entityManager=$this->getDoctrine()->getManager();

            $zamowienie->setStatus($status);
            $zamowienie->setDataOddania(new \DateTime($data_oddania));
            $entityManager->persist($zamowienie);
            $entityManager->flush();
            var_dump($id);
            return new JsonResponse(array('id'=>'this is reponse'));
        }
        return new Response("This is not ajax",400);
/*        return $this->render('worker/mojPlan.html.twig',['tab'=>$tab]);*/
    }
}