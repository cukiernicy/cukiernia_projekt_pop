<?php


namespace App\Controller;
use App\Entity\AdresKlienta;
use App\Form\UserType;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="user_registration")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $token = $this->get('security.token_storage')->getToken()->getUser();//pobranie adresu email lub anon. jeżeli nie jest zalogowany
        $flag=0;
        // 1) build the form
        // 1.a) rejestracja
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);

        $repositoryUser=$this->getDoctrine()->getRepository(User::class);
        $query2=$repositoryUser->createQueryBuilder('u')
            ->where('u.email LIKE :email')
            ->setParameter('email', $user->getEmail())
            ->getQuery();
        $emailChecker=$query2->getResult();
        /*dd($emailChecker);*/


        if($emailChecker!=null)
        {
            $flag=1;
            /*return new Response('Formularz nie jest wypełniony poprawnie. Taki login już jest zajęty');*/
            return $this->render(
                'registration/register.html.twig',
                array('form' => $form->createView(), 'flag'=>$flag, 'user'=>$token)
            );
        }

        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $email1=$user->getEmail();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('app_login');
            //return $this->redirectToRoute('configure', ['email1'=>$email1]);
            /*$response = $this->forward('App\Controller\KonfiguracjaKontaController::configure', [
                'email1'  => $email1,
            ]);
            return $response;*/


        }


        return $this->render(
            'registration/register.html.twig',
            array('form' => $form->createView(), 'flag'=>$flag, 'user'=>$token)
        );


    }
}