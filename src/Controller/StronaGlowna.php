<?php


namespace App\Controller;


use App\Entity\Oferta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class StronaGlowna extends AbstractController
{
    /**
     * @Route ("/", name="strona_glowna")
     */
    public function show()
    {

        $token=$this->get("security.token_storage")->getToken()->getUser();
        $repository = $this->getDoctrine()->getRepository(Oferta::class);
        $oferta = $repository->findAll();
        return $this->render("stronaGlowna.html.twig", ['user'=>$token, 'oferta'=>$oferta]);
    }
}