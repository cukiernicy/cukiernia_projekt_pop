<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $token = $this->get('security.token_storage')->getToken()->getUser();//pobranie adresu email lub anon. jeżeli nie jest zalogowany

         if ($this->getUser() && $this->getUser()->getRoles()=='ROLE_ADMIN') {
             dump($this->getUser());
             return $this->redirectToRoute('customer');
         }
         else
         {
             if ($this->getUser() && $this->getUser()->getRoles()=='ROLE_USER')
             {
                 return $this->redirectToRoute('customer');
             }

         }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error, 'user'=>$token]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
