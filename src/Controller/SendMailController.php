<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class SendMailController extends AbstractController
{
    /**
     * @Route("send-mail", name="sendMail")
     */
    public function sendMail(MailerInterface $mailer){
        $email = (new Email())
            ->from('ewus.bartek@gmail.com')
            ->to('barwieleba@gmail.com')
            ->subject('Whaddup')
            ->text('whazaaaaaaa')
            ->html('<p>Eloooo</p>');

        $mailer->send($email);

        return $this->redirectToRoute('app_login');
    }
}