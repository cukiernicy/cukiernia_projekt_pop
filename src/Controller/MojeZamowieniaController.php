<?php


namespace App\Controller;


use App\Entity\Oferta;
use App\Entity\Zamowienie;
use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MojeZamowieniaController extends AbstractController
{
    /**
     * @Route("/moje_zamowienia", name="moje_zamowienia")
     */
    public function show()
    {
        $token=$this->get("security.token_storage")->getToken()->getUser();

        $repository=$this->getDoctrine()->getRepository(Zamowienie::class);
        $zamowienie=$repository->findBy(array('user_id_usera'=>$token));

        $repository2=$this->getDoctrine()->getRepository(Oferta::class);
        $oferta=$repository2->findAll();

        return $this->render("moje_zamowienia/moje_zamowienia.html.twig", ['user'=>$token, 'zamowienia'=>$zamowienie, 'oferty'=>$oferta]);
    }

    /**
     * @Route ("/moje_zamowienia/{slug}", name="moje_zamowienia_edytuj")
     */
    public function edytuj(string $slug, Request $request)
    {
        $token=$this->get("security.token_storage")->getToken()->getUser();
        $repository = $this->getDoctrine()->getRepository(Zamowienie::class);
        $zamowienie=$repository->find($slug);

        $zamowienie_form=new Zamowienie();
        $zamowienie_form->setStatus($zamowienie->getStatus());
        $zamowienie_form->setUserIdUsera($zamowienie->getUserIdUsera());
        $zamowienie_form->setOfertaIdOferty($zamowienie->getOfertaIdOferty());
        $zamowienie_form->setDataPrzyjecia($zamowienie->getDataPrzyjecia());
        $zamowienie_form->setWaga($zamowienie->getWaga());
        $zamowienie_form->setDataOddania($zamowienie->getDataOddania());


        $form=$this->createFormBuilder($zamowienie_form)
            ->add('oferta_id_oferty', TextType::class, [ 'label'=>'Nazwa ciasta ', 'disabled'=>'on'])
            ->add('data_przyjecia', DateType::class, ['disabled'=>'on'])
            ->add('waga', NumberType::class, ['label'=>'Waga (kg)'])
            /*->add('status', NumberType::class, ['disabled'=>'on'])*/
            /*->add('user_id_usera', TextType::class, ['disabled'=>'on'])*/
            //->add('oferta_id_oferty', TextType::class, [ 'label'=>'Nazwa ciasta '])
            ->add('Edytuj', SubmitType::class, ['label' => ' Edytuj'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            //dd($task->getWaga());
            $entityManager = $this->getDoctrine()->getManager();
            $zamowienie->setWaga($task->getWaga());//przechwycenie z formularza wagi
            $entityManager->persist($zamowienie);
            $entityManager->flush();
            return $this->redirectToRoute('oferta');
        }

        return $this->render("moje_zamowienia/edytuj.html.twig",['slug'=>$slug, 'form'=>$form->createView(), 'user'=>$token, 'obrazek'=>$zamowienie->getOfertaIdOferty()->getUrlObrazka() ]);
    }

    /**
     * @Route ("/moje_zamowienia/usun/{slug}", name="moje_zamowienia_usun")
     */
    public function usun( string $slug)
    {
        $repository = $this->getDoctrine()->getRepository(Zamowienie::class);
        $zamowienie=$repository->find($slug);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($zamowienie);
        $entityManager->flush();
        return $this->redirect('/moje_zamowienia');
    }
}
