<?php


namespace App\Controller;


use App\Entity\AdresKlienta;
use App\Entity\User;
use App\Form\AddressType;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class KonfiguracjaKontaController extends AbstractController
{
    /**
     * @Route ("/configure", name="configure")
     */
    public function configure(Request $request_2, $email1)
    {
        $token = $this->get('security.token_storage')->getToken()->getUser();//pobranie adresu email lub anon. jeżeli nie jest zalogowany
        $adres=new AdresKlienta();
        $form=$this->createForm(AddressType::class, $adres);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request_2);
        if ($form->isSubmitted() /*&& $form->isValid()*/) {
            $repository = $this->getDoctrine()->getRepository(User::class);
            $user1 = $repository->findOneBy(['email' => $email1]);

            $adres->setKlientIdKlienta($user1);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($adres);
            $entityManager->flush();
            return $this->redirectToRoute('app_login');
        }
        return $this->render(
            'registration/configure.html.twig',
            array('form' => $form->createView(),  'user'=>$token)
        );
    }
}