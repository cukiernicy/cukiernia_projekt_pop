<?php


namespace App\Controller;


use App\Entity\ZamowieniePracownik;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ZamowieniaZrealizowaneController extends AbstractController
{
    /**
     * @Route("/zrealizowane_zamowienia", name="zrealizowane_zamowienia")
     */
    public function zrealizowane_zamowienia()
    {
        $token=$this->get('security.token_storage')->getToken()->getUser();
        if(!$token->getRoles()){
            $error_message = 'Nie masz odpowiednich uprawnień.';
            return $this->render('error_msg/error_msg.html.twig', ['user'=>$token, 'error_message'=>$token->getRoles()]);
        }
        $repositoryZamowieniaPracownika=$this->getDoctrine()->getRepository(ZamowieniePracownik::class);
        $zamowienia_pracownika=$repositoryZamowieniaPracownika->findAll();
        return $this->render('worker/zrealizowane_zamowienia.html.twig', ['user'=>$token,'zamowienia_pracownika'=>$zamowienia_pracownika]);
    }
}