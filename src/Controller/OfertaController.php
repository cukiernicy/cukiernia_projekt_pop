<?php


namespace App\Controller;


use App\Entity\Oferta;
use App\Entity\Zamowienie;
use Doctrine\DBAL\Types\FloatType;
use Doctrine\DBAL\Types\IntegerType;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

class OfertaController extends AbstractController
{
    /**
     * @Route ("/oferta", name="oferta")
     */
    public function oferta()
    {
        $token = $this->get('security.token_storage')->getToken()->getUser();
        $repository = $this->getDoctrine()->getRepository(Oferta::class);
        $oferta = $repository->findAll();
        return $this->render('oferta/oferta.html.twig', ['oferta'=>$oferta, 'user'=>$token]);

    }
    /**
     * @Route("/oferta/{slug}", name="blog_post")
     */
    public function show(int $slug, Request $request)
    {
        $token = $this->get('security.token_storage')->getToken()->getUser();
        $repository=$this->getDoctrine()->getRepository(Oferta::class);
        $oferta=$repository->findBy(array('id'=>$slug));

        $repository1=$this->getDoctrine()->getRepository(Oferta::class);
        $zamowienie=$repository->find($slug);

        $task=new Zamowienie();
        $task->setDataPrzyjecia(new \DateTime('today'));
        $task->setOfertaIdOferty($oferta[0]);
        $task->setStatus(0);
        $task->setUserIdUsera($token);


        $form=$this->createFormBuilder($task)
           /* ->add('data_przyjecia', DateType::class, ['disabled'=>'on'])*/
            ->add('waga', NumberType::class, ['label'=>'Waga (kg)', 'required' => true ])
            /*->add('status', NumberType::class, ['disabled'=>'on'])*/
            /*->add('user_id_usera', TextType::class, ['disabled'=>'on'])*/
           /* ->add('oferta_id_oferty', TextType::class, ['disabled'=>'on', 'label'=>'Nazwa ciasta '])*/
            ->add('Dodaj', SubmitType::class, ['label' => ' Dodaj do koszyka'])
            ->getForm();
        ;
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            //
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute('moje_zamowienia');
        }

        return $this->render('oferta/zamowienie.html.twig', ['slug'=>$slug, 'oferty'=>$oferta, 'form'=>$form->createView(), 'user'=>$token, 'zamowienie'=>$zamowienie]);
    }

}