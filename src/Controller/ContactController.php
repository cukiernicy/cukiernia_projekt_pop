<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Zamowienie;
use App\Form\ContactType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index()
    {
        $token = $this->get('security.token_storage')->getToken()->getUser();
        $form = $this->createForm(ContactType::class);
        $repositoryContact=$this->getDoctrine()->getRepository(Contact::class);
        $contact=$repositoryContact->findAll();

        return $this->render('contact/index.html.twig', ['kontakt'=>$contact, 'user'=>$token]);
    }
    /**
     * @Route("/get_content", name="get_content", methods={"POST"})
     * @param Request $request
     */
    public function getContent(Request $request, MailerInterface $mailer)
    {
        if($request->isXMLHttpRequest())
        {
            $tytul=$request->request->get('tytul');
            $wiadomosc=$request->request->get('wiadomosc');
            $email=$request->request->get('email');
            $token = $this->get('security.token_storage')->getToken()->getUser();
            $repositoryContact=$this->getDoctrine()->getRepository(Contact::class);
            $entityManager=$this->getDoctrine()->getManager();
            /*$contact=new Contact();
            $contact->setTytul($tytul);
            $contact->setWiadomosc($wiadomosc);
            $contact->setEmail($email);
            $entityManager->persist($contact);
            $entityManager->flush();*/
            $email = (new TemplatedEmail())
                ->from($email)
                ->to('ewus.bartek@gmail.com')
                ->subject($tytul)
                ->text($wiadomosc);

            $mailer->send($email);

            return new JsonResponse(array('$tytul'=>'this is reponse'));
        }
        return new Response("This is not ajax",400);
    }
}
