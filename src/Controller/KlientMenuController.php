<?php


namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class KlientMenuController extends AbstractController
{
    /**
     * @Route("/menu", name="customer")
     */
    public function menu()
    {
        $token = $this->get('security.token_storage')->getToken()->getUser();//pobranie adresu email lub anon. jeżeli nie jest zalogowany
        return $this->render('menu/customer.html.twig', ['user'=>$token]);
    }
}