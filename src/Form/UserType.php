<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use App\Entity\User;
use App\Entity\AdresKlienta;
class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('required' => true))
            ->add('surname', TextType::class, array('required' => true))
            ->add('email', EmailType::class, array('required' => true))
            ->add('tel', TelType::class, array('required' => true))
            ->add('password', PasswordType::class, array('required' => true))
            ->add('miejscowosc', TextType::class, array('required' => true))
            ->add('ulica', TextType::class, array('required' => true))
            ->add('kodPocztowy',TextType::class, array('required' => true))
            ->add('nrDomu', TextType::class, array('required' => true))
            ->add('nrLokalu', TextType::class, array('required' => false))


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}