<?php


namespace App\Validator\Constraints;
use Symfony\Component\Validator\Constraint;
/**
 * @Annotation
 */
class ContainsAlphanumeric
{
    public $massage='Imie nie może zawierać liter i musi się zaczynać z dużej litery';
    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }



}