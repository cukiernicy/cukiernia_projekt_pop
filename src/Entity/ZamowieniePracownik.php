<?php

namespace App\Entity;

use App\Repository\ZamowieniePracownikRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ZamowieniePracownikRepository::class)
 */
class ZamowieniePracownik
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $pracownik_id_pracownika;

    /**
     * @ORM\ManyToOne(targetEntity=Zamowienie::class)
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $zamowienie_id_zamowienia;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPracownikIdPracownika(): ?User
    {
        return $this->pracownik_id_pracownika;
    }

    public function setPracownikIdPracownika(?User $pracownik_id_pracownika): self
    {
        $this->pracownik_id_pracownika = $pracownik_id_pracownika;

        return $this;
    }

    public function getZamowienieIdZamowienia(): ?Zamowienie
    {
        return $this->zamowienie_id_zamowienia;
    }

    public function setZamowienieIdZamowienia(?Zamowienie $zamowienie_id_zamowienia): self
    {
        $this->zamowienie_id_zamowienia = $zamowienie_id_zamowienia;

        return $this;
    }
}
