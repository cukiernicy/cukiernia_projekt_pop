<?php

namespace App\Entity;

use App\Repository\ZamowienieRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ZamowienieRepository::class)
 */
class Zamowienie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $data_przyjecia;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $data_oddania;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=Oferta::class)
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $oferta_id_oferty;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $user_id_usera;

    /**
     * @ORM\Column(type="float")
     */
    private $waga;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDataPrzyjecia(): ?\DateTimeInterface
    {
        return $this->data_przyjecia;
    }

    public function setDataPrzyjecia(\DateTimeInterface $data_przyjecia): self
    {
        $this->data_przyjecia = $data_przyjecia;

        return $this;
    }

    public function getDataOddania(): ?\DateTimeInterface
    {
        return $this->data_oddania;
    }

    public function setDataOddania(?\DateTimeInterface $data_oddania): self
    {
        $this->data_oddania = $data_oddania;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getOfertaIdOferty(): ?Oferta
    {
        return $this->oferta_id_oferty;
    }

    public function setOfertaIdOferty(?Oferta $oferta_id_oferty): self
    {
        $this->oferta_id_oferty = $oferta_id_oferty;

        return $this;
    }

    public function getUserIdUsera(): ?User
    {
        return $this->user_id_usera;
    }

    public function setUserIdUsera(?User $user_id_usera): self
    {
        $this->user_id_usera = $user_id_usera;

        return $this;
    }
    public function __toString()
    {
        return ''.$this->id;
    }

    public function getWaga(): ?float
    {
        return $this->waga;
    }

    public function setWaga(float $waga): self
    {
        $this->waga = $waga;

        return $this;
    }
}
