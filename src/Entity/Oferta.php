<?php

namespace App\Entity;

use App\Repository\OfertaRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=OfertaRepository::class)
 * @Vich\Uploadable()
 */
class Oferta
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $nazwa_oferty;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $opis_oferty;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url_obrazka;

    /**
     * @Vich\UploadableField(mapping="obraz", fileNameProperty="url_obrazka")
     */
    private $obrazek;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="float")
     */
    private $cena_za_kg;

    public function __construct()
    {
        $this->updatedAt=new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNazwaOferty(): ?string
    {
        return $this->nazwa_oferty;
    }

    public function setNazwaOferty(string $nazwa_oferty): self
    {
        $this->nazwa_oferty = $nazwa_oferty;

        return $this;
    }

    public function getOpisOferty(): ?string
    {
        return $this->opis_oferty;
    }

    public function setOpisOferty(?string $opis_oferty): self
    {
        $this->opis_oferty = $opis_oferty;

        return $this;
    }

    public function getUrlObrazka(): ?string
    {
        return $this->url_obrazka;
    }

    public function setUrlObrazka(?string $url_obrazka): self
    {
        $this->url_obrazka = $url_obrazka;

        return $this;
    }

    public function getObrazek()
    {
        return $this->obrazek;
    }

    public function setObrazek( $obrazek): void
    {
        $this->obrazek = $obrazek;

        if($obrazek instanceof UploadedFile)
        {
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getUpdatedAt():  ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt)
    {
        $this->updatedAt=new \DateTime();
        return $this;
    }

    public function __toString()
    {
        return $this->nazwa_oferty;
    }

    public function getCenaZaKg(): ?float
    {
        return $this->cena_za_kg;
    }

    public function setCenaZaKg(float $cena_za_kg): self
    {
        $this->cena_za_kg = $cena_za_kg;

        return $this;
    }


}
