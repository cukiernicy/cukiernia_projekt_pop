<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     *
     */
    private $email;



    /**
     * @ORM\Column(type="json")
     */
    private $roles = ["ROLE_USER"];



    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     *  @Assert\Length(
     *      min = 8,
     *      minMessage = "Twoje hasło musi mieć conajmniej 8 znaków")
     */
    private $password;

//    /**
//     * @var string
//     */
//    private $plainPassword;


    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern     = "/^[A-ZŁ][a-z]*$/",
     *     message="Imie musi być z dużej litery"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern     = "/^[A-ZŁ][a-ząęółśżźćń]*$/",
     *     message="Nazwisko musi być z dużej litery"
     * )
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern     = "/^([0-9]{3})+([-| ])?+([0-9]{3})+([-| ])?+([0-9]{3})$/i",
     *     message="Telefon jest nieprawidłowy powinień składać się z 9 cyfr"
     * )
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $miejscowosc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ulica;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nr_domu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nr_lokalu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $kod_pocztowy;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER


        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    //hashowanie w easyadmin
    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        //$this->plainPassword = null;
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }


    //moje funkcje
    public function __toString()
    {
        // TODO: Implement __toString() method
        return $this->email;
    }

    public function getMiejscowosc(): ?string
    {
        return $this->miejscowosc;
    }

    public function setMiejscowosc(?string $miejscowosc): self
    {
        $this->miejscowosc = $miejscowosc;

        return $this;
    }

    public function getUlica(): ?string
    {
        return $this->ulica;
    }

    public function setUlica(?string $ulica): self
    {
        $this->ulica = $ulica;

        return $this;
    }

    public function getNrDomu(): ?string
    {
        return $this->nr_domu;
    }

    public function setNrDomu(?string $nr_domu): self
    {
        $this->nr_domu = $nr_domu;

        return $this;
    }

    public function getNrLokalu(): ?string
    {
        return $this->nr_lokalu;
    }

    public function setNrLokalu(?string $nr_lokalu): self
    {
        $this->nr_lokalu = $nr_lokalu;

        return $this;
    }

    public function getKodPocztowy(): ?string
    {
        return $this->kod_pocztowy;
    }

    public function setKodPocztowy(?string $kod_pocztowy): self
    {
        $this->kod_pocztowy = $kod_pocztowy;

        return $this;
    }

}
