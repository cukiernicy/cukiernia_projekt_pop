<?php

namespace App\Entity;

use App\Repository\PlanRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlanRepository::class)
 */
class Plan
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $poniedzialek;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $wtorek;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sroda;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $czwartek;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $piatek;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $pracownik_id_pracownika;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPoniedzialek(): ?string
    {
        return $this->poniedzialek;
    }

    public function setPoniedzialek(?string $poniedzialek): self
    {
        $this->poniedzialek = $poniedzialek;

        return $this;
    }

    public function getWtorek(): ?string
    {
        return $this->wtorek;
    }

    public function setWtorek(?string $wtorek): self
    {
        $this->wtorek = $wtorek;

        return $this;
    }

    public function getSroda(): ?string
    {
        return $this->sroda;
    }

    public function setSroda(?string $sroda): self
    {
        $this->sroda = $sroda;

        return $this;
    }

    public function getCzwartek(): ?string
    {
        return $this->czwartek;
    }

    public function setCzwartek(?string $czwartek): self
    {
        $this->czwartek = $czwartek;

        return $this;
    }

    public function getPiatek(): ?string
    {
        return $this->piatek;
    }

    public function setPiatek(?string $piatek): self
    {
        $this->piatek = $piatek;

        return $this;
    }

    public function getPracownikIdPracownika(): ?User
    {
        return $this->pracownik_id_pracownika;
    }

    public function setPracownikIdPracownika(?User $pracownik_id_pracownika): self
    {
        $this->pracownik_id_pracownika = $pracownik_id_pracownika;

        return $this;
    }
}
