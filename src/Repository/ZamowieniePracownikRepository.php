<?php

namespace App\Repository;

use App\Entity\ZamowieniePracownik;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ZamowieniePracownik|null find($id, $lockMode = null, $lockVersion = null)
 * @method ZamowieniePracownik|null findOneBy(array $criteria, array $orderBy = null)
 * @method ZamowieniePracownik[]    findAll()
 * @method ZamowieniePracownik[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZamowieniePracownikRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ZamowieniePracownik::class);
    }

    // /**
    //  * @return ZamowieniePracownik[] Returns an array of ZamowieniePracownik objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('z.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ZamowieniePracownik
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
